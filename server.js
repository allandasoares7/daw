const express = require('express')
const app = express()
const fs = require('fs')

app.use(express.static(__dirname + '/public'))

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/html/index.html')
})

app.get('/eu', (req, res) => {
    res.sendFile(__dirname + '/public/html/eu.html')
})

app.get('/aluno', (req, res) => {
    res.sendFile(__dirname + '/public/html/aluno.html')
})

app.get('/duvida', (req, res) => {
    res.sendFile(__dirname + '/public/html/duvida.html')
})

app.get('/confirmacao', (req, res) => {
    var duv = req.query.duvida
    var email = req.query.email

    res.send(`A dúvida: ${duv} foi enviada com sucesso. </br>
     Responderemos no e-mail: ${email} </br> </br> <a href="/">
    ↣Voltar a página inicial↢ </a>`)
})

app.get('/busca', (req, res) => {
    var nome = req.query.query
    let aux = []
    
    fs.readFile(__dirname + "/alunos.json", "utf8", (error, dado) => {
        const alunos = JSON.parse(dado)

        for(i in alunos){
            console.log(alunos[i].nome)

            if(alunos[i].nome.includes(nome)){
                aux.push(alunos[i])
            }
        }
        return res.json(aux)
    })
})

app.listen(3000, () => {
    console.log("Server is running")
})